<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >=4.1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >=4.1.0 |
| <a name="provider_external"></a> [external](#provider\_external) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_access_key.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_access_key) | resource |
| [aws_iam_openid_connect_provider.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_policy.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy) | resource |
| [external_external.thumbprint](https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/external) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_account"></a> [allow\_account](#input\_allow\_account) | Account ID to allow access to | `string` | `""` | no |
| <a name="input_audiences"></a> [audiences](#input\_audiences) | Audience string found in Pipeline Settings (make sure it's an array) | `list(string)` | n/a | yes |
| <a name="input_iam_policy"></a> [iam\_policy](#input\_iam\_policy) | n/a | `string` | `"{\"Version\" : \"2012-10-17\",\"Statement\" : [{\"Sid\" : \"AdminPermissions\",\"Effect\" : \"Allow\",\"Action\" : [\"*\"],\"Resource\" : \"*\"}]}"` | no |
| <a name="input_identity_provider_arn"></a> [identity\_provider\_arn](#input\_identity\_provider\_arn) | Identity provider ARN (set if prexisting provider exists) | `string` | `""` | no |
| <a name="input_name"></a> [name](#input\_name) | Role Name | `string` | n/a | yes |
| <a name="input_url"></a> [url](#input\_url) | Identity provider URL found in Pipeline Settings | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_keys"></a> [keys](#output\_keys) | n/a |
<!-- END_TF_DOCS -->