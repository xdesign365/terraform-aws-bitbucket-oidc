variable "name" {
  type = string
  description = "Role Name"
}

variable "url" {
  type = string
  description = "Identity provider URL found in Pipeline Settings"
}

variable "audiences" {
  type = list(string)
  description = "Audience string found in Pipeline Settings (make sure it's an array)"
}

variable "iam_policy" {
  type = string
  default = "{\"Version\" : \"2012-10-17\",\"Statement\" : [{\"Sid\" : \"AdminPermissions\",\"Effect\" : \"Allow\",\"Action\" : [\"*\"],\"Resource\" : \"*\"}]}"
}

variable "allow_account" {
  default = ""
  description = "Account ID to allow access to"
}

variable "create_user" {
  default = false
  description = "Create debug user (only needed for local testing)"
}

variable "identity_provider_arn" {
  type = string
  description = "Identity provider ARN (set if prexisting provider exists)"
  default = ""
}